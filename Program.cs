﻿using System;
/// <summary>
/// 
/// </summary>
class Program {
	public static void Main(string[] args) {
		double[] re = new double[]{1,2,3,4,5,6,7,8};
		double[] im = new double[]{0,0,0,0,0,0,0,0};
		double[,] re2d = new double[,] {
			{ 1, 2, 3, 4 },
			{ 4, 1, 2, 3 },
			{ 3, 4, 1, 2 },
			{ 2, 3, 4, 1 },
		};
		double[,] im2d = new double[,] {
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
		};
		FFT fft = new FFT(re.Length);
		Console.WriteLine("Forward Fourier Transform:");
		fft.Transform(ref re, ref im, false);
		Write(re, im);
		Console.WriteLine("Backward Fourier Transform:");
		fft.Transform(ref re, ref im, true);
		Write(re, im);
		Console.WriteLine("Forward Fourier Transform:");
		FFT.Transform2D(ref re2d, ref im2d, false);
		Write(re2d, im2d);
		Console.WriteLine("Backward Fourier Transform:");
		FFT.Transform2D(ref re2d, ref im2d, true);
		Write(re2d, im2d);
		Console.Write("Press any key to exit...");
		Console.ReadKey(true);
	}
	private static void Write(double[] re, double[] im) {
		int length = re.Length;
		for (int i = 0; i < length; i++)
			Console.Write("{0:F0}x{1:F0}j\t", re[i], im[i]);
		Console.WriteLine();
	}
	private static void Write(double[,] re, double[,] im) {
		int xl = re.GetLength(1);
		int yl = re.GetLength(0);
		for (int x = 0; x < xl; x++) {
			for (int y = 0; y < yl; y++) {
				Console.Write("{0:F0}x{1:F0}j\t",re[x,y], im[x,y]);
			}
			Console.WriteLine();
		}
	}
}