﻿using System;
/// <summary>
/// Fast Fourier transform.
/// Dmytro Magar 25.07.2015
/// </summary>
public class FFT {
	#region DATA
	private int length;
	private int stages;
	private int[] table;
	#endregion DATA
	#region CONSTRUCTORS
	public FFT(int length) {
		this.length = length;
		this.stages = (int)Math.Log(length, 2);
		this.table = CreateReverseTable(length, stages);
	}
	#endregion CONSTRUCTORS
	#region METHODS
	public void Transform(ref double[] re, ref double[] im, bool inverse = false) {
		if(inverse) Scale(ref re, ref im, length);
		Transform(ref re, ref im, length, stages, inverse);
		Sort(ref re, ref im, table,  length);
	}
	#endregion METHODS
	#region FUNCTIONS
	public static void Transform2D(ref double[,] re, ref double[,] im, bool inverse) {
		int x = re.GetLength(1);
		int y = re.GetLength(0);
		double[] are = new double[x];
		double[] aim = new double[x];
		FFT fft = new FFT(x);
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				are[j] = re[i, j];
				aim[j] = im[i, j];
			}
			fft.Transform(ref are, ref aim, inverse);
			for (int j = 0; j < x; j++) {
				re[i, j] = are[j];
				im[i, j] = aim[j];
			}
		}
		are = new double[y];
		aim = new double[y];
		fft = new FFT(y);
		for (int i = 0; i < x; i++) {
			for (int n = 0; n < y; n++) {
				are[n] = re[n, i];
				aim[n] = im[n, i];
			}
			fft.Transform(ref are, ref aim, inverse);
			for (int j = 0; j < y; j++) {
				re[j, i] = are[j];
				im[j, i] = aim[j];
			}
		}
	}
	public static void Sort(ref double[] re, ref double[] im, int[] table, int length) {
		for (int i = 0; i < length; i++) {
			int j = table[i];
			if(j > i) {
				double num = re[i];
				double num2 = im[i];
				re[i] = re[j];
				re[j] = num;
				im[i] = im[j];
				im[j] = num2;
			}
		}
	}
	public static void Scale(ref double[] re, ref double[] im, int length) {
		double scale = 1.0 / length;
		for (int i = 0; i < length; i++) {
			re[i] *= scale;
			im[i] *= scale;
		}
	}
	public static void Transform(ref double[] re, ref double[] im, int length, int stages, bool inverse = false) {
		double wSize = Math.PI / length * (inverse ? 2.0 : -2.0);
		for (int stage = 0, spacing = length; stage < stages; stage++, spacing >>= 1) {
			double angle = wSize * (1 << stage);
			double cos = Math.Cos(angle);
			double sin = Math.Sin(angle);
			for (int start = 0; start < length; start += spacing) {
				double are = 1.0;
				double aim = 0.0;
				for (int count = 0, max = spacing >> 1, top = start, bot = top + max; count < max; count++, top = start + count, bot = top + max) {
					double bre = re[top] - re[bot];
					double bim = im[top] - im[bot];
					re[top] = re[top] + re[bot];
					im[top] = im[top] + im[bot];
					re[bot] = bre * are - bim * aim;
					im[bot] = bre * aim + bim * are;
					double tre = are * cos - aim * sin;
					aim = are * sin + aim * cos;
					are = tre;
				}
			}
		}
	}
	public static int[] CreateReverseTable(int length, int stages) {
		int[] numArray = new int[length];
		for (int num = 0, num2 = 0, num3 = 0; num < length; numArray[num] = num3, num2 = ++num, num3 = 0)
			for (int num4 = 0; num4 < stages; num4++) {
			num3 <<= 1;
			num3 |= num2 & 0x0001;
			num2 >>= 1;
		}
		return numArray;
	}
	#endregion FUNCTIONS
}